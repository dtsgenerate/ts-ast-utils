# ts-ast-utils

A simpler & friendlier way to interact with the Typescript AST.

## visitAllChildren

Lets you specify a visitor for a specific kind of node or for all of them. It will visit each node in the AST and apply the visitor function accordingly.

## createFromString

Easily creates an AST from a string.
